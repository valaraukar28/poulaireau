﻿using UnityEngine;
using System.Collections;

public class SpaceShipController : MonoBehaviour 
{
	// Player life
	public	int			playerLife;
	private	int			currentLife;

    // Autofire
    public  bool        autofire;
    public  float       autofireRate;
    private float       autofireCurrentTime;

    // Projectiles
	public	GameObject	projectileContainer;
	public	GameObject	projectilePrefab;

	// Movements
	public	float		horizontalSpeed;
	public	float		verticalSpeed;

	public	float		horizontalLimit;
	public	float		verticalLimit;

	private	Transform	trans;
	// Use this for initialization
	void Start () 
	{
		this.trans	=	this.transform;
	}
	
	// Update is called once per frame
	void Update () 
	{
		this.MovementUpdate ();

		bool shoot  = Input.GetButtonDown("Fire1") || Input.GetButtonDown("Fire2") || 
                      Input.GetButtonDown("Fire3") || Input.GetKeyDown(KeyCode.Space);

        if (this.autofire)
        {
            bool    autoFireKey    =   Input.GetKey(KeyCode.Space) && !shoot;
            if (autoFireKey)
            {
                this.autofireCurrentTime += Time.deltaTime;
                if (this.autofireCurrentTime > this.autofireRate)
                {
                    this.autofireCurrentTime -= this.autofireRate;
                    shoot = true;
                }
            }
        }
		if (shoot)
		{
			// Create a new projectile
			Shoot();
		}
	}

	private void Shoot()
	{
		GameObject 	shot 		= 	Instantiate(this.projectilePrefab) as GameObject;
		Transform	shotTrans	=	shot.transform;
		if (this.projectileContainer)
			shotTrans.parent	=	this.projectileContainer.transform;
		shotTrans.position 		= 	this.transform.position;
		
		BulletShot shotScript 	= 	shot.GetComponent<BulletShot>();
		shotScript.speed 		= 	new Vector2(0, 25);
	}

	private void	MovementUpdate ()
	{
		float 	horizontalTranslation	=	Input.GetAxis ("Vertical") * this.horizontalSpeed;
		float	verticalTranslation		=	-Input.GetAxis ("Horizontal") * this.verticalSpeed;
		
		this.trans.Translate ( horizontalTranslation, verticalTranslation, 0f);
		
		Vector3	position				=	this.trans.localPosition;
		bool	limit					=	false;
		
		if (position.x 		<	-horizontalLimit)
		{
			position.x		=	-horizontalLimit;
			limit			=	true;
		}
		else if (position.x > 	horizontalLimit)
		{
			position.x		=	horizontalLimit;
			limit			=	true;
		}
		
		if (position.y		<	-verticalLimit)
		{
			position.y		=	-verticalLimit;
			limit			=	true;
		}
		else if (position.y >	verticalLimit)
		{
			position.y		=	verticalLimit;
			limit			=	true;
		}
		
		if (limit)
			this.trans.localPosition	=	position;
	}
}
