﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer (typeof (ActivatePopUpAttribute))]
public class ActivatePopUpDrawer : PropertyDrawer 
{
	enum ActivateOption
	{
		Active,
		Desactive
	}

	public override void OnGUI ( Rect position,
	                            SerializedProperty property,
	                            GUIContent label) 
	{
		label = EditorGUI.BeginProperty (position, label, property);
		
		ActivateOption enumValue =
			(property.boolValue == true ? ActivateOption.Active : ActivateOption.Desactive);
		EditorGUI.BeginChangeCheck ();
		enumValue = (ActivateOption)EditorGUI.EnumPopup (position, label, enumValue);
		if (EditorGUI.EndChangeCheck ())
			property.boolValue = (enumValue == ActivateOption.Active ? true : false);
		
		EditorGUI.EndProperty ();
	}
}
