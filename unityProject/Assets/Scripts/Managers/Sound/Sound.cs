﻿using UnityEngine;
using System.Collections;


public class Sound : MonoBehaviour 
{
	public	int				mSoundTypeIndex;
	private	AudioSource 	mAudio;
	private	SoundType		mType;

	// Use this for initialization
	void Start () 
	{
		// Get the Audio Source
		this.mAudio = this.GetComponent<AudioSource>();

		// Get the Sound Type
		this.GetSoundType ();
	}

	void OnDestroy ()
	{
		this.mType.volumeHandler 	-= 	this.ChangeVolume;
		this.mType.muteHandler		-=	this.ChangeMute;
	}

	void ChangeVolume (float volume)
	{
		if (!this.mType.mute)
			this.mAudio.volume = volume;
	}	

	void ChangeMute (bool active)
	{
		float volume = 0f;
		if (!active)
			volume = this.mType.volume;

		this.mAudio.volume = volume;
	}

	void GetSoundType ()
	{
		SoundManager	manager	=	SoundManager.GetInstance ();

		if (manager.mSounds == null || manager.mSounds.Count == 0)
		{
			Debug.LogError ("No Sound Type declared");
			return;
		}
		if (this.mSoundTypeIndex >= manager.mSounds.Count)
		{
			Debug.LogWarning ("The Sound Type doesn't exist anymore");
			this.mSoundTypeIndex = 	0;
		}

		this.mType				=	manager.mSounds[this.mSoundTypeIndex];

		this.mType.volumeHandler 	+= 	this.ChangeVolume;
		this.mType.muteHandler		+=	this.ChangeMute;
	}
}
