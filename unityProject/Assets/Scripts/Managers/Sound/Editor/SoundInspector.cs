﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor (typeof (Sound) )]
[CanEditMultipleObjects]
public class SoundInspector : Editor 
{
	private	SoundManager		soundManager;
	private	string[]			soundTypeNames;

	private	SerializedProperty	index;

	private void	OnEnable ()
	{
		this.GetSoundManager ();

		this.index	=	serializedObject.FindProperty ("mSoundTypeIndex");
	}

	public override void OnInspectorGUI ()
	{
		EditorGUI.BeginChangeCheck ();
		{
			this.index.intValue = EditorGUILayout.Popup ("Type", this.index.intValue, this.soundTypeNames);
		}

		if(EditorGUI.EndChangeCheck ())
		{
		}
	}

	private	void	GetSoundManager ()
	{
		// The better way ?!
		this.soundManager	=	FindObjectOfType<SoundManager>();
		
		List<string> names	=	new List<string>();
		foreach (SoundType type in this.soundManager.mSounds)
			names.Add (type.name);
		
		this.soundTypeNames	=	names.ToArray ();
	}
}
