﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomPropertyDrawer (typeof (SoundTypeAttribute))]
public class SoundTypeDrawer : PropertyDrawer
{
	private SoundManager	soundManager;
	private string[]		soundTypeNames;

	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
		label	=	EditorGUI.BeginProperty (position, label, property);
		{
			int currentIndex	=	property.intValue;
			this.GetSoundManager ();

			EditorGUI.BeginChangeCheck ();
			{
				currentIndex	=	EditorGUI.Popup (position, "Type", currentIndex, this.soundTypeNames);
			}
			if (EditorGUI.EndChangeCheck ())
				property.intValue	=	currentIndex;
		}
		EditorGUI.EndProperty ();
	}

	private	void	GetSoundManager ()
	{
		// The better way ?!
		this.soundManager	=	GameObject.FindObjectOfType<SoundManager>();
		
		List<string> names	=	new List<string>();
		foreach (SoundType type in this.soundManager.mSounds)
			names.Add (type.name);
		
		this.soundTypeNames	=	names.ToArray ();
	}
}
