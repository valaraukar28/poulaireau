﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor (typeof (SoundManager) )]
public class SoundManagerInspector : Editor 
{
	private	GUIStyle			style			=	new GUIStyle ();
	private	SerializedProperty	mSounds;

	void OnEnable ()
	{
		this.style.richText		=	true;
		this.style.alignment	=	TextAnchor.MiddleCenter;

		this.mSounds			=	serializedObject.FindProperty ("mSounds");
	}

	public override void OnInspectorGUI ()
	{
		serializedObject.Update ();

		this.OnSoundsGUI (this.mSounds);

		serializedObject.ApplyModifiedProperties ();
	}

	private void ProgressBar (float value, string label)
	{
		// Get a rect for the progress bar using the same margins as a textfield:
		Rect rect = GUILayoutUtility.GetRect (18, 18, "TextField");
		EditorGUI.ProgressBar (rect, value, label);
		EditorGUILayout.Space ();
	}

	private void OnSoundsGUI (SerializedProperty property)
	{
		if (!property.isArray)
			return;

		if (property.arraySize.Equals (0))
		{
			EditorGUILayout.HelpBox ("There is no Sound Type. Do you want to create a new one ?", MessageType.Info);
			if (GUILayout.Button ("Create a New Sound Type") )
				property.InsertArrayElementAtIndex (0);
		}

		EditorGUILayout.BeginVertical ("Box");
		{
			EditorGUILayout.BeginHorizontal ();
			{
				GUILayout.FlexibleSpace ();
				EditorGUILayout.LabelField ("<size=15><color=purple><b>Sound Types</b></color></size>", this.style);
				GUILayout.FlexibleSpace ();
			}
			EditorGUILayout.EndHorizontal ();
			for (int i = 0; i < property.arraySize; i++)
			{
				EditorGUILayout.Space();
				this.OnSoundTypeGUI (property.GetArrayElementAtIndex (i));
			}

			EditorGUILayout.BeginHorizontal ();
			{
				if (GUILayout.Button ("Add Sound Type"))
					property.InsertArrayElementAtIndex (property.arraySize);
			}
			EditorGUILayout.EndHorizontal ();
		}
		EditorGUILayout.EndVertical ();


	}

	private void OnSoundTypeGUI (SerializedProperty property)
	{
		EditorGUILayout.BeginVertical ( "OL Box", GUILayout.MaxHeight (100f) );
		{
			property.FindPropertyRelative ("name").stringValue = EditorGUILayout.TextField (
																	new GUIContent (""), 
																	property.FindPropertyRelative ("name").stringValue,
                                                                	"OL Title");

			EditorGUILayout.Separator ();

			EditorGUILayout.PropertyField (property.FindPropertyRelative ("mute"));

			this.ProgressBar (property.FindPropertyRelative ("volume").floatValue, "volume");
			EditorGUILayout.PropertyField (property.FindPropertyRelative ("volume"));

			EditorGUILayout.Separator ();

			EditorGUILayout.BeginHorizontal ();
			{
				GUILayout.FlexibleSpace ();
				if (GUILayout.Button ("Remove"))
					property.DeleteCommand ();
			}
			EditorGUILayout.EndHorizontal ();
		}
		EditorGUILayout.EndVertical ();
	}
}
