﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SoundType
{
	#region Members
	public	string	name;
	[Range (0f, 1f)]
	public	float	volume;
	[ActivatePopUp]
	public	bool	mute;
	#endregion

	#region Handlers
	public 	SoundManager.VolumeChange	volumeHandler;
	public	SoundManager.MuteChange		muteHandler;
	#endregion

	#region Constructors
	public SoundType () : this("Default", 1f, false)
	{
	}

	public SoundType (string mName) : this (mName, 1f, false)
	{
	}

	public SoundType (string mName, float mVol, bool mMute)
	{
		this.name	= 	mName;
		this.volume	=	mVol;
		this.mute	=	mMute;
	}
	#endregion
}
