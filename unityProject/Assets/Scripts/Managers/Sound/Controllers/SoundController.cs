﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour 
{
	[SoundType]
	public	int				mSoundTypeIndex;
	private SoundType		mType;


	protected	void 	GetSoundType ()
	{
		SoundManager	manager	=	SoundManager.GetInstance ();
		
		if (manager.mSounds == null || manager.mSounds.Count == 0)
		{
			Debug.LogError ("No Sound Type declared");
			return;
		}
		if (this.mSoundTypeIndex >= manager.mSounds.Count)
		{
			Debug.LogWarning ("The Sound Type doesn't exist anymore");
			this.mSoundTypeIndex = 	0;
		}
		
		this.mType				=	manager.mSounds[this.mSoundTypeIndex];
	}

	protected 	void 	VolumeChange (float volume)
	{
		// Change the volume
		this.mType.volume	=	volume;

		// If there are listeners, inform them 
		if (this.mType.volumeHandler != null)
			this.mType.volumeHandler(volume);
	}

	protected	void	MuteChange (bool active)
	{
		// Change the mute
		this.mType.mute	=	active;
		// If there are listeners, inform them 
		if (this.mType.muteHandler != null)
			this.mType.muteHandler(active);
	}
}
