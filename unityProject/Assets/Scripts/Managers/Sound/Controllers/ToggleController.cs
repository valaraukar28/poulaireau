﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ToggleController : SoundController 
{
	public	Toggle	toggle;

	// Use this for initialization
	void Start () 
	{
		// Get the Sound Type
		this.GetSoundType ();

		if (this.toggle)
			this.toggle.onValueChanged.AddListener (this.MuteChange);
	}

	void OnDestroy ()
	{
		if (this.toggle)
			this.toggle.onValueChanged.RemoveListener (this.MuteChange);
	}
}
