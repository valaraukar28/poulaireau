﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SliderController : SoundController 
{
	public	Slider	slider;
	// Use this for initialization
	void Start () 
	{
		// Get the Sound Type
		this.GetSoundType ();

		if (this.slider)
			this.slider.onValueChanged.AddListener (this.VolumeChange);
	}

	void OnDestroy ()
	{
		if (this.slider)
			this.slider.onValueChanged.RemoveListener (this.VolumeChange);
	}
}
