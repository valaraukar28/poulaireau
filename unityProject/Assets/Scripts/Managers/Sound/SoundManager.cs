﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour 
{
	#region Static
	private static SoundManager instance;
	public	static SoundManager GetInstance ()
	{
		return instance;
	}
	#endregion

	// Delegates
	public	delegate	void	VolumeChange (float volume);
	public	delegate	void	MuteChange (bool mute);

	// List of SoundType
	public	List<SoundType>		mSounds	=	new List<SoundType>();

	void Awake ()
	{
		// Always keep the manager
		DontDestroyOnLoad (this.gameObject);

		if (instance == null)
			instance = this;
		else
			Debug.LogWarning ("You can't have two instances of the Sound Manager");
	}

}
