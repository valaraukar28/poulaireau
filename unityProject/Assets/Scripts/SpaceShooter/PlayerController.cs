﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundary 
{
	public float xMin, xMax, yMin, yMax;
}

public class PlayerController : MonoBehaviour
{
	public float speed;
	public float tilt;
	public Boundary boundary;

	public GameObject shot;
	public Transform shotSpawn;
	public float fireRate;
	 
	private float nextFire;

	void Start ()
	{
		Camera mainCam = Camera.main;
		if (mainCam)
		{
			float ratio  = (float)Screen.height / (float)Screen.width;

			if (ratio >= 1f)
			{
				boundary.yMax = mainCam.orthographicSize - 1f;
				boundary.yMin = -boundary.yMax;
				boundary.xMin = boundary.yMin / ratio;
				boundary.xMax = boundary.yMax / ratio;
			}
			else
			{
				boundary.xMax = mainCam.orthographicSize - 1f;
				boundary.xMin = -boundary.xMax;
				boundary.yMin = boundary.xMin / ratio;
				boundary.yMax = boundary.xMax / ratio;
			}
		}
	}
	
	void Update ()
	{
		if (Input.GetButton("Fire1") && Time.time > nextFire) 
		{
			nextFire = Time.time + fireRate;
			Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
			GetComponent<AudioSource>().Play ();
		}
	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, moveVertical, 0.0f);
		GetComponent<Rigidbody>().velocity = movement * speed;

		GetComponent<Rigidbody>().position = new Vector3
		(
			Mathf.Clamp (GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax), 
			Mathf.Clamp (GetComponent<Rigidbody>().position.y, boundary.yMin, boundary.yMax), 
			0.0f
		);
		
		GetComponent<Rigidbody>().rotation = Quaternion.Euler (270.0f, 0.0f, GetComponent<Rigidbody>().velocity.x * -tilt);
	}
}
