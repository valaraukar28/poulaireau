﻿using UnityEngine;
using System.Collections;

public class MenuController: MonoBehaviour 
{
	private const string MAIN = "Main";

	public	GameObject	mainMenuPanel;
	public	GameObject	settingsPanel;

	private	RectTransform	mainMenuPanelT;
	private RectTransform	settingsPanelT;
	// Use this for initialization
	void Start () 
	{
		if (this.mainMenuPanel)
			this.mainMenuPanelT = this.mainMenuPanel.GetComponent<RectTransform> ();
		if (this.settingsPanel)
			this.settingsPanelT = this.settingsPanel.GetComponent<RectTransform> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void OnPlayClick ()
	{
		Application.LoadLevel (MAIN);
	}

	public void OnSettingsClick ()
	{
		Vector3 dest = 482f * Vector3.left;
		StartCoroutine (MovePanel (this.mainMenuPanelT, this.settingsPanelT, dest, 1.0f));
	}

	public void OnBackSettingsClick ()
	{
		Vector3 dest = 482f * Vector3.right;
		StartCoroutine (MovePanel (this.mainMenuPanelT, this.settingsPanelT, dest, 1.0f));
	}

	private IEnumerator MovePanel (RectTransform panel1, RectTransform panel2, Vector3 distance, float time)
	{
		float t = 0f;
		Vector3 move;
		while (t < time) 
		{
			move = distance * Time.deltaTime / time;
			panel1.Translate (move);
			panel2.Translate (move);
			t += Time.deltaTime;
			yield return new WaitForEndOfFrame ();
		}
	}
}
