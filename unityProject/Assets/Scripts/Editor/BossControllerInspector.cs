﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor (typeof (BossController))]
[CanEditMultipleObjects]
public class BossControllerInspector : Editor
{
    private SerializedProperty healthProp;
    private SerializedProperty currentHealthProp;
    private SerializedProperty speedProp;

    public void OnEnable ()
    {
        this.healthProp = serializedObject.FindProperty("health");
        this.currentHealthProp = serializedObject.FindProperty("currentHealth");
        this.speedProp = serializedObject.FindProperty("speed");
    }

    public override void OnInspectorGUI()
    {
        // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
        serializedObject.Update();

        EditorGUILayout.PropertyField(this.healthProp);
        // Only show the damage progress bar if all the objects have the same damage value:
        if (!currentHealthProp.hasMultipleDifferentValues)
            ProgressBar(currentHealthProp.intValue / (float)this.healthProp.intValue, "Current Health");

		EditorGUILayout.PropertyField (this.speedProp);
        // Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
        serializedObject.ApplyModifiedProperties();
    }

    private void ProgressBar (float value, string label)
    {
        // Get a rect for the progress bar using the same margins as a textfield:
		Rect rect = GUILayoutUtility.GetRect (18, 18, "TextField");
		EditorGUI.ProgressBar (rect, value, label);
		EditorGUILayout.Space ();
    }
}
