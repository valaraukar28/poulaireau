﻿using UnityEngine;
using System.Collections;

public class BulletShot : MonoBehaviour 
{
	public 	Vector2 	speed = Vector2.zero;

	private	Renderer	rend;

	void Start()
	{
		this.rend	=	this.GetComponent<Renderer>();
	}
	
	void Update()
	{
		// Destroy when outside the screen
		if (this.rend != null && this.rend.isVisible == false)
		{
			Destroy(this.gameObject);
		}
	}
	
	void FixedUpdate()
	{
		GetComponent<Rigidbody2D>().velocity = speed;
	}
}
