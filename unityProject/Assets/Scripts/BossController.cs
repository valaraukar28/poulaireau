﻿using UnityEngine;
using System.Collections;

public class BossController : MonoBehaviour 
{

	public int health = 100;
    public int currentHealth = 100;
    public float speed = 2f;

    private Vector3 movementTarget;

    void Start()
    {
      NewMoveTarget();
    }

    void Update()
    {
      if (GetComponent<Collider2D>().OverlapPoint(movementTarget))
      {
        NewMoveTarget();
      }

      Vector2 direction = (movementTarget - this.transform.position);
      direction.Normalize();
      //this.transform.Translate(direction * speed * Time.deltaTime);
    }

    private void NewMoveTarget()
    {
      movementTarget = new Vector3(
        Random.Range(0.5f, 0.9f),
        Random.Range(0.1f, 0.9f),
        0);

      movementTarget = Camera.main.ViewportToWorldPoint(movementTarget);
      movementTarget.z = 0;
    }

    void OnTriggerEnter2D(Collider2D otherCollider)
    {
      // Collision with player projectile
      BulletShot playerShot = otherCollider.GetComponent<BulletShot>();

      if (playerShot != null)
      {
        health--;

        DestroyObject(playerShot.gameObject);

        // Flash red
        StartCoroutine(FlashRed());

        if (health == 0)
        {
          Destroy(this.gameObject);
        }
      }
    }

    private IEnumerator FlashRed()
    {
      SpriteRenderer sprite = GetComponentInChildren<SpriteRenderer>();

      sprite.color = Color.red;

      yield return new WaitForSeconds(0.05f);

      sprite.color = Color.white;

      yield return null;
    }

}
